'use strict';

const NodeI18N = require('../src/NodeI18N');

const i18n = new NodeI18N('en-US');
i18n.load('../locales', ['shard']).then(() => {
	console.log(i18n.strings.get('en-US'));
}).catch(e => {
	console.log(e);
});
