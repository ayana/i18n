'use strict';

module.exports = undefined;

if (global.process) {
	const fs = require('fs');
	const path = require('path');
	const util = require('util');

	const axios = require('axios');

	let tar;
	try {
		tar = require('tar');
	} catch (e) {
		// Ignore
	}

	const access = util.promisify(fs.access);
	const mkdir = util.promisify(fs.mkdir);
	const readdir = util.promisify(fs.readdir);
	const readFile = util.promisify(fs.readFile);
	const stat = util.promisify(fs.stat);
	const unlink = util.promisify(fs.unlink);

	const I18N = require('./I18N');

	class NodeI18N extends I18N {
		constructor(location, source = 'en-US', components = []) {
			if (!location) throw new Error(`NodeI18N: Please specify location of translations.`);
			super(source);

			this.settings = new Map();

			this.location = location;

			if (typeof components === 'string') components = [components];
			this.components = components;
		}

		async load() {
			try {
				await access(this.location, fs.constants.F_OK);
			} catch (e) {
				throw new Error(`NodeI18N: Locales location odes not exist: ${this.location}`);
			}

			const locales = await readdir(this.location);

			// load locale settings.json file
			const promises = [];
			for (const locale of locales) {
				promises.push(this.loadLocale(this.location, locale, this.components));
			}
			await Promise.all(promises);
		}

		async loadLocale(location, locale, components) {
			let loc = path.resolve(location, locale, 'strings');
			// special exception for source locale (top level strings)
			if (locale === 'strings') loc = path.resolve(location, 'strings');

			// verify directory has 'strings/' in it
			try {
				await access(loc, fs.constants.F_OK);
			} catch (e) {
				return;
			}

			const settings = await this._loadSettings(loc);
			if (!settings) return;

			// don't load locale if not enabled
			if (settings.enabled !== true) return;
			this.settings.set(locale === 'strings' ? this.source : locale, settings);

			// load common strings
			const commonStrings = await this._loadCommon(loc);

			// load components
			const promises = [];
			for (const component of components) {
				promises.push(this._loadComponent(loc, component));
			}
			const results = await Promise.all(promises);

			const componentStrings = {};
			for (const strings of results) {
				Object.assign(componentStrings, strings);
			}

			this.addTranslations(locale === 'strings' ? this.source : locale, Object.assign({}, commonStrings, componentStrings));
		}

		async _loadAsJSON(file) {
			let data = await readFile(file, { encoding: 'utf8' });

			try {
				data = JSON.parse(data);
			} catch (e) {
				throw new Error(`NodeI18N: Failed to parse JSON file ${file}`);
			}

			return data;
		}

		async _loadSettings(location) {
			const stats = await stat(location);
			if (!stats.isDirectory()) return null;

			const settingsFile = path.resolve(location, 'settings.json');
			try {
				access(settingsFile, fs.constants.F_OK);
			} catch (e) {
				throw new Error(`NodeI18N: Could not find settings.json in ${location}`);
			}

			return this._loadAsJSON(settingsFile);
		}

		async _loadCommon(location) {
			const common = path.resolve(location, 'common.json');
			try {
				await access(common, fs.constants.F_OK);
			} catch (e) {
				return {};
			}

			return this._loadAsJSON(common);
		}

		async _loadComponent(location, component) {
			try {
				await access(path.resolve(location, component), fs.constants.F_OK);
			} catch (e) {
				return {};
			}

			return this._loadRecursive(path.resolve(location, component));
		}

		async _loadEntry(location, entry, strings = {}) {
			const entryPath = path.resolve(location, entry);
			const stats = await stat(entryPath);

			if (stats.isDirectory()) {
				await this._loadRecursive(path.resolve(location, entry), strings);
			} else {
				Object.assign(strings, await this._loadAsJSON(entryPath));
			}
		}

		async _loadRecursive(location, strings = {}) {
			const entries = await readdir(location);

			const promises = [];
			for (const entry of entries) {
				promises.push(this._loadEntry(location, entry, strings));
			}
			await Promise.all(promises);

			return strings;
		}

		async download(url) {
			if (!tar) throw new Error('NodeI18N: Cannot download locales if package \'tar\' is not installed');

			if (!fs.existsSync(this.location)) await mkdir(this.location);

			const tarball = path.resolve(this.location, '.locales.tar.gz');
			await new Promise(async (resolve, reject) => {
				try {
					(await axios({
						url,
						method: 'get',
						responseType: 'stream',
					})).data.pipe(fs.createWriteStream(tarball)).on('close', () => {
						resolve();
					}).on('error', e => {
						reject(e);
					});
				} catch (e) {
					reject(e);
				}
			});

			if (!fs.existsSync(tarball)) throw new Error('NodeI18N: Failed to download new locales (file not found)');

			await new Promise((resolve, reject) => {
				fs.createReadStream(tarball).pipe(tar.x({
					strip: 1,
					C: this.localesRoot,
				})).on('close', () => {
					resolve();
				}).on('error', e => {
					reject(e);
				});
			});

			await unlink(tarball);
		}
	}

	module.exports = NodeI18N;
}
