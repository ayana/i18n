'use strict';

const IntlMessageFormat = require('intl-messageformat');

class I18N {
	constructor(source) {
		this.stats = new Map();
		this.strings = new Map();

		this.source = source || 'en-US';
	}

	getPercentTranslated(lang) {
		if (!lang) throw new Error(`I18N: Please specify a target lang.`);

		if (!this.strings.has(this.source) || !this.stats.has(this.source)) {
			throw new Error(`I18N: Source locale '${this.source}' is not loaded`);
		}
		if (!this.strings.has(lang) || !this.stats.has(lang)) {
			throw new Error(`I18N: Compare locale '${lang}' is not loaded`);
		}

		const sourceCount = this.stats.get(this.source);
		const compareCount = this.stats.get(lang);

		return ((compareCount / sourceCount) * 100).toFixed(2);
	}

	/**
	 * Adds and merges translation data
	 * @param {string} lang - locale string
	 * @param {Object} strings - Flat key-value pair object of translation strings
	 *
	 * @returns {boolean} true
	 */
	addTranslations(lang, strings = {}) {
		if (!lang) throw new Error('I18N: Please specify a target lang.');

		if (!this.strings.has(lang)) this.strings.set(lang, new Map());
		const map = this.strings.get(lang);

		for (const [key, value] of Object.entries(strings)) {
			if (!value) continue;

			map.set(key, strings[key]);
		}

		this.stats.set(lang, map.size);
		return true;
	}

	/**
	 * Remove all translation data for given locale
	 * @param {string} lang - locale string
	 *
	 * @returns {boolean} true
	 */
	removeTranslations(lang) {
		if (!lang) throw new Error('I18N: Please specify a target lang.');

		if (!this.strings.has(lang)) return true;

		this.strings.delete(lang);
		this.stats.delete(lang);
		return true;
	}

	hasTranslation(lang, key) {
		if (!lang) throw new Error('I18N: Please specify a target lang.');
		if (!this.strings.has(lang)) throw new Error('I18N: Target language does not exists.');

		const strings = this.strings.get(lang);
		return strings.has(key);
	}

	translate(lang, key, repl = {}, returnNull = false) {
		if (!lang || !key) throw new Error(`I18N: Target lang and translation key are required.`);

		if (!this.strings.has(lang)) {
			// See if source locale is loaded
			if (!this.strings.has(this._source)) {
				return returnNull ? null : 'I18N: Non-existent locale requested and source locale not loaded.';
			}

			// Fallback to source locale if the requested locale could not be found
			lang = this.source;
		}

		// Get strings of the selected locale
		let strings = this.strings.get(lang);
		// If the current locale doesn't have the key
		if (!strings.has(key)) {
			// Fallback to source locale
			strings = this.strings.get(this.source);
			// If the source locale doesn't have the key
			if (!strings.has(key)) {
				if (returnNull) return null;

				// Error out
				return this.translate(lang, 'I18N_RESOLVE_FAILED', { key }, true) ||
					`Failed to resolve translation "${key}"`;
			}
		}

		// Get string for translation
		const str = strings.get(key);

		// Format it
		try {
			return new IntlMessageFormat(str, lang).format(repl);
		} catch (e) {
			if (returnNull) return null;

			const escaped = e.message.replace(/`/g, '');

			// Error out because something went wrong
			return this.translate(lang, 'I18N_FORMAT_FAILED', { key, error: escaped }, true) ||
				`Failed to format translation "${key}": ${escaped}`;
		}
	}
}

module.exports = I18N;
